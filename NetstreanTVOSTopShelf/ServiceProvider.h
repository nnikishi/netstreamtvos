//
//  ServiceProvider.h
//  NetstreanTVOSTopShelf
//
//  Created by Nickolai Nikishin on 12/4/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TVServices/TVServices.h>

@interface ServiceProvider : NSObject <TVTopShelfProvider>


@end

