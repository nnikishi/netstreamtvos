//
//  ServiceProvider.m
//  NetstreanTVOSTopShelf
//
//  Created by Nickolai Nikishin on 12/4/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import "ServiceProvider.h"

@interface ServiceProvider ()

@end

@implementation ServiceProvider


- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - TVTopShelfProvider protocol

- (TVTopShelfContentStyle)topShelfStyle {
    // Return desired Top Shelf style.
    return TVTopShelfContentStyleSectioned;
}

- (NSArray *)topShelfItems {
    // Create an array of TVContentItems.
    
    TVContentItem *sectionItem = [[TVContentItem alloc] initWithContentIdentifier:[[TVContentIdentifier alloc] initWithIdentifier:@"NetstreamTVOS.channels" container:nil]];
    sectionItem.title = @"Last viewed channels";
    
    sectionItem.topShelfItems = [self arrayItemsForTopShelf];
    
    return @[sectionItem];
}


- (NSArray *)arrayItemsForTopShelf {
    
    __block NSMutableArray *arrayItemsForTopShelf = [NSMutableArray array];
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.NetstreamTVOS"];
    NSArray *channels = [shared objectForKey:@"topShelfChannels"];
    [channels enumerateObjectsUsingBlock:^(NSData *encodedObject, NSUInteger idx, BOOL * _Nonnull stop) {
       
        NSDictionary *channel = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        NSString *channelName = channel[@"channelName"];
        NSString *channelId = channel[@"channelId"];
        NSString *channelItentifier = [NSString stringWithFormat:@"NetstreamTVOS.channels.%@", channelName];
        NSString *channelURLImage = channel[@"channelURLImage"];
        NSString *playURL = channel[@"channelPlayURL"];
        
        TVContentItem *item = [[TVContentItem alloc] initWithContentIdentifier:[[TVContentIdentifier alloc] initWithIdentifier:channelItentifier container:nil]];
        item.imageURL = [NSURL URLWithString:channelURLImage];
        item.imageShape = TVContentItemImageShapeSquare;
        
        NSURLComponents *components = [[NSURLComponents alloc] init];
        components.scheme = @"NetstreamTVOS";
        components.path = playURL;
     //   components.queryItems = @[[NSURLQueryItem queryItemWithName:@"id" value:playURL]];
        item.displayURL = components.URL;
        
        [arrayItemsForTopShelf addObject:item];
    }];
    
    return arrayItemsForTopShelf;
    
}

@end
