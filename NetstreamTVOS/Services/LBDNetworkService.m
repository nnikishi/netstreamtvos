//
//  LBDNetworkService.m
//  NetstreamTVOS
//
//  Created by Dmitry on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import "LBDNetworkService.h"
#import "AFHTTPRequestOperationManager.h"
#import "LBDBroadcast.h"
#import "LBDChannel.h"

static NSString *const kLBDNetworkServicePairingTokenURL    = @"http://u8182.vdc.netstream.com:8081/ib/trusted/requestPairingToken/13:13:13:13:13:14/1000053/1";
static NSString *const kLBDNetworkServiceLoginURL           = @"http://u8182.vdc.netstream.com:8081/ib/public/authenticate/%@/1";
static NSString *const kLBDNetworkServiceLiveBroadcastsURL  = @"http://u8182.vdc.netstream.com:8081/ib/auth/epg/now-playing?channels=%@&includeNextPlaying=false";
static NSString *const kLBDNetworkServiceStreamingURL       = @"http://u8182.vdc.netstream.com:8081/ib/auth/stream/tv/%@";
static NSString *const kLBDNetworkServiceChannelsURL        = @"http://u8182.vdc.netstream.com:8081/ib/auth/tv/channels";


typedef void (^__nullable LBDServiceChannelsCompletion)(NSArray<LBDChannel *> * __nonnull channels);
typedef void (^__nullable LBDServiceTokenCompletion)();
typedef void (^__nullable LBDServiceLoginCompletion)();

@interface LBDNetworkService ()
@property (nonatomic, copy, nullable) NSString *pairingToken;
@property (nonatomic, copy, nullable) NSString *authToken;
@end

@implementation LBDNetworkService

#pragma mark -
#pragma mark Init

- (id)init {
	if (self = [super init]) {
		AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
		manager.responseSerializer = [AFJSONResponseSerializer serializer];
		manager.requestSerializer  = [AFJSONRequestSerializer serializer];
		[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
	}
	return self;
}

#pragma mark -
#pragma mark Public

- (void)performBroadcastsRequestWithCompletion:(LBDServiceLiveBroadcastsCompletion)aCompletion {
	[self p_performLoginRequestWithCompletion:^{
		[self p_performChannelsRequestWithCompletion:^(NSArray *channels) {
            
            __block NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            
            [channels enumerateObjectsUsingBlock:^(LBDChannel *channel, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString *channelID = channel.ID;
                NSString *name = channel.channelName;
                [dict setObject:name forKey:channelID];
            }];
            
			NSArray *channelsIDs = [channels valueForKeyPath:@"ID"];
			NSString *channelsIDsString = [channelsIDs componentsJoinedByString:@","];
			[self p_performLiveBroadcastsRequestForChannels:channelsIDsString channels:dict withCompletion:aCompletion];
		}];
	}];
}

- (void)performStreamingRequestForBroadcast:(LBDBroadcast *)aBroadcast andCompletion:(LBDServiceStreamingCompletion)aCompletion {
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	[manager.requestSerializer setValue:_pairingToken forHTTPHeaderField:@"X-Pairing-Token"];
	[manager.requestSerializer setValue:_authToken forHTTPHeaderField:@"X-Auth-Token"];
	NSString *URLString = [NSString stringWithFormat:kLBDNetworkServiceStreamingURL, aBroadcast.channelID];
	[manager GET:URLString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
		if ([responseObject isKindOfClass:[NSDictionary class]]) {
			NSDictionary *responseDictionary = (NSDictionary *)responseObject;
			NSString *streamingURLString = responseDictionary[@"url"];
			aCompletion([NSURL URLWithString:streamingURLString]);
		}

	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		NSLog(@"Error: %@", error);
	}];
}

#pragma mark -
#pragma mark Private

- (void)p_performLiveBroadcastsRequestForChannels:(nonnull NSString *)aChannelsIDs channels:(nonnull NSDictionary *)channels withCompletion:(LBDServiceLiveBroadcastsCompletion)aCompletion {
	[self p_performLoginRequestWithCompletion:^{
		AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
		[manager.requestSerializer setValue:_pairingToken forHTTPHeaderField:@"X-Pairing-Token"];
		[manager.requestSerializer setValue:_authToken forHTTPHeaderField:@"X-Auth-Token"];
		NSString *URLString = [NSString stringWithFormat:kLBDNetworkServiceLiveBroadcastsURL, aChannelsIDs];
		[manager GET:URLString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
			if ([responseObject isKindOfClass:[NSArray class]]) {
				NSMutableArray *broadcasts = [NSMutableArray new];
				for (NSDictionary *objectDictionary in (NSArray *)responseObject) {
					LBDBroadcast *broadcast = [LBDBroadcast broadcastWithDictionary:objectDictionary andChannelName:channels[objectDictionary[@"channelId"]]];
					[broadcasts addObject:broadcast];
				}
				aCompletion(broadcasts);
			}

		} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
			NSLog(@"Error: %@", error);
		}];
	}];
}

- (void)p_performChannelsRequestWithCompletion:(LBDServiceChannelsCompletion)aCompletion {
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	[manager.requestSerializer setValue:_pairingToken forHTTPHeaderField:@"X-Pairing-Token"];
	[manager.requestSerializer setValue:_authToken forHTTPHeaderField:@"X-Auth-Token"];
	[manager GET:kLBDNetworkServiceChannelsURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
		if ([responseObject isKindOfClass:[NSArray class]]) {
			NSMutableArray *channels = [NSMutableArray new];
			for (NSDictionary *objectDictionary in (NSArray *)responseObject) {
				LBDChannel *channel = [LBDChannel channelWithDictionary:objectDictionary];
				[channels addObject:channel];
			}

			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.mobileAvailable == YES"];
			NSArray *filteredChannels = [channels filteredArrayUsingPredicate:predicate];
			aCompletion(filteredChannels);
		}

	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		NSLog(@"Error: %@", error);
	}];
}

- (void)p_performLoginRequestWithCompletion:(LBDServiceLoginCompletion)aCompletion {
	[self p_performRequestForPairingTokenWithCompletion:^() {
		NSString *URLString = [NSString stringWithFormat:kLBDNetworkServiceLoginURL, _pairingToken];
		AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
		[manager.requestSerializer setValue:@"1" forHTTPHeaderField:@"tenantId"];
		[manager POST:URLString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
			if ([responseObject isKindOfClass:[NSDictionary class]]) {
				NSDictionary *responseDictionary = (NSDictionary *)responseObject;
				self.authToken = responseDictionary[@"authToken"];
				aCompletion();
			}

		} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
			NSLog(@"Error: %@", error);
		}];

	}];
}

- (void)p_performRequestForPairingTokenWithCompletion:(LBDServiceTokenCompletion)aCompletion {
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	[manager GET:kLBDNetworkServicePairingTokenURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
		if ([responseObject isKindOfClass:[NSDictionary class]]) {
			NSDictionary *responseDictionary = (NSDictionary *)responseObject;
			self.pairingToken = responseDictionary[@"pairingToken"];
			aCompletion();
		}

	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		NSLog(@"Error: %@", error);
	}];
}

@end
