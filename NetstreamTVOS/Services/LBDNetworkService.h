//
//  LBDNetworkService.h
//  NetstreamTVOS
//
//  Created by Dmitry on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LBDBroadcast;

typedef void (^__nullable LBDServiceLiveBroadcastsCompletion)(NSArray<LBDBroadcast *> * __nonnull broadcasts);
typedef void (^__nullable LBDServiceStreamingCompletion)(NSURL *__nonnull streamingURL);

@interface LBDNetworkService : NSObject
- (void)performBroadcastsRequestWithCompletion:(LBDServiceLiveBroadcastsCompletion)aCompletion;
- (void)performStreamingRequestForBroadcast:(nonnull LBDBroadcast *)aBroadcast andCompletion:(LBDServiceStreamingCompletion)aCompletion;
@end
