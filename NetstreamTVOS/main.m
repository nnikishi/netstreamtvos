//
//  main.m
//  NetstreamTVOS
//
//  Created by Nickolai Nikishin on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LBDAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LBDAppDelegate class]));
    }
}
