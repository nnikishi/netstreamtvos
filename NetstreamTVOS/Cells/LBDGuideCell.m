//
//  LBDGuideCell.m
//  NetstreamTVOS
//
//  Created by Nickolai Nikishin on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import "LBDGuideCell.h"
#import "LBDBroadcast.h"
#import "UIImageView+AFNetworking.h"

@interface LBDGuideCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageBroadcast;
@property (weak, nonatomic) IBOutlet UILabel *labelBroadcastTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelStartEndTime;
@property (weak, nonatomic) IBOutlet UIImageView *imageChannelLogo;


@end

@implementation LBDGuideCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.imageChannelLogo.image = nil;
    self.imageBroadcast.image = nil;
}

- (void)updateWithBroadcast:(LBDBroadcast *)aBroadcast {
    self.labelBroadcastTitle.text = aBroadcast.title;
    self.labelStartEndTime.text = [NSString stringWithFormat:@"%@ - %@", [aBroadcast startTimeRepresentation], [aBroadcast endTimeRepresentation]];
    NSURL *broadcastThumbnailURL = [aBroadcast broadcastThumbnailURL];
    [self.imageBroadcast setImageWithURL:broadcastThumbnailURL];
    NSURL *channelLogoURL = [aBroadcast channelLogoURL];
    [self.imageChannelLogo setImageWithURL:channelLogoURL];
}

+ (NSString *)cellIdentifier {
    return NSStringFromClass([self class]);
}
+ (CGFloat)cellDefaultHeight {
    return 380.0f;
}

@end
