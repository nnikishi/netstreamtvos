//
//  LBDGuideCell.h
//  NetstreamTVOS
//
//  Created by Nickolai Nikishin on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LBDBroadcast;

@interface LBDGuideCell : UITableViewCell

+ (NSString *)cellIdentifier;
+ (CGFloat)cellDefaultHeight;

- (void)updateWithBroadcast:(LBDBroadcast *)aBroadcast;

@end
