//
//  AppDelegate.h
//  NetstreamTVOS
//
//  Created by Nickolai Nikishin on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LBDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

