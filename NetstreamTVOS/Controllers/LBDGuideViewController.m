//
//  LBDGuideApplicationController.m
//  NetstreamTVOS
//
//  Created by Nickolai Nikishin on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import "LBDGuideViewController.h"
#import "LBDBroadcast.h"
#import "LBDGuideCell.h"
#import "LBDNetworkService.h"
#import <AVKit/AVKit.h>

@interface LBDGuideViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imageCompanyLogo;
@property (weak, nonatomic) IBOutlet UILabel *labelNowDate;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) LBDNetworkService *networkService;
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) UITapGestureRecognizer *playPauseRecognizer;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) NSArray *arrayTopShelfChannels;

@end

@implementation LBDGuideViewController

- (LBDNetworkService *)networkService {
    if (!_networkService) {
        _networkService = [LBDNetworkService new];
    }
    
    return _networkService;
}

- (NSArray *)arrayTopShelfChannels {
    
    if (!_arrayTopShelfChannels) {
        NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.NetstreamTVOS"];
        _arrayTopShelfChannels = [shared objectForKey:@"topShelfChannels"];
    }

    return _arrayTopShelfChannels;
}

- (void)dealloc {
    [_timer invalidate];
    _timer = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.playPauseRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePlayPauseTap:)];
    self.playPauseRecognizer.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypePlayPause]];
    [self.view addGestureRecognizer:self.playPauseRecognizer];
    [self updateLabelNowDate];
    [self prepareUpdateLabelNowDateTimer];
    [self fetchData];
}

- (void)handlePlayPauseTap:(id)sender {
	if ([UIScreen.mainScreen.focusedView isKindOfClass:[LBDGuideCell class]]) {
		LBDGuideCell *cell = (LBDGuideCell *)UIScreen.mainScreen.focusedView;
        NSIndexPath *focusedIndexPath = [self.tableView indexPathForCell:cell];
        if (focusedIndexPath) {
            [self playBroadcastItem:self.dataArray[focusedIndexPath.row]];
        }
        
    };
}

- (void)fetchData {
   typeof(self) wself = self;
    [self.networkService performBroadcastsRequestWithCompletion:^(NSArray<LBDBroadcast *> * _Nonnull broadcasts) {
        wself.dataArray = broadcasts;
        [wself.tableView reloadData];
        
    }];
}

- (void)prepareUpdateLabelNowDateTimer {
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc]
                              initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components =
    [gregorian components:NSCalendarUnitSecond fromDate:today];
    NSInteger second = [components second];
    [self performSelector:@selector(startUpdateLabelNowDateTimer) withObject:nil afterDelay:60-second];
}

- (void)updateLabelNowDate {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy hh:mm"];
    NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
    self.labelNowDate.text = dateString;
}

- (void)startUpdateLabelNowDateTimer {
    [self updateLabelNowDate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:60.0 target:self selector:@selector(updateLabelNowDate) userInfo:nil repeats:YES];
}

#pragma mark -
#pragma mark UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LBDGuideCell *cell = [tableView dequeueReusableCellWithIdentifier:[LBDGuideCell cellIdentifier]];
    [cell updateWithBroadcast:self.dataArray[indexPath.row]];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger broadcastsCount = [self.dataArray count];
    
    if (broadcastsCount>0) {
        [self.activityIndicator stopAnimating];
        self.activityIndicator.hidden = YES;
        self.tableView.hidden = NO;
    } else {
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
        self.tableView.hidden = YES;
    }
    
    return broadcastsCount;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self playBroadcastItem:self.dataArray[indexPath.row]];
}

- (void)playBroadcastItem:(LBDBroadcast *)broadcastItem {
    AVPlayerViewController *pvc = [[AVPlayerViewController alloc] initWithNibName:nil bundle:nil];
    
    typeof(self) wself = self;
    
    [self.networkService performStreamingRequestForBroadcast:broadcastItem andCompletion:^(NSURL * _Nonnull streamingURL) {
        pvc.player = [AVPlayer playerWithURL:streamingURL];
        [wself presentViewController:pvc animated:YES completion:^{
            [pvc.player play];
        }];
        
        [wself addBroadcastChannelToTopShelf:broadcastItem playURL:[streamingURL absoluteString]] ;

    }];
    
   
}

- (void)addBroadcastChannelToTopShelf:(LBDBroadcast *)broadcastItem playURL:(NSString *)playURL {
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.NetstreamTVOS"];

    
    if (self.arrayTopShelfChannels) {
        
        __block BOOL channelAlreadyExists = NO;
        
        [self.arrayTopShelfChannels enumerateObjectsUsingBlock:^(NSData *encodedObject, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSDictionary *channel = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
            
            if ([channel[@"channelId"] isEqualToString:broadcastItem.channelID]) {
                channelAlreadyExists = YES;
                *stop = YES;
            }
        }];
        
        
        if (!channelAlreadyExists) {
            NSMutableArray *array = [NSMutableArray arrayWithArray:self.arrayTopShelfChannels];
            
            NSDictionary *objectToAdd = @{@"channelName" : broadcastItem.channelName, @"channelId" : broadcastItem.channelID, @"channelURLImage" : [broadcastItem.channelLogoURL absoluteString], @"channelPlayURL" : playURL};
            NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:objectToAdd];

            [array addObject:encodedObject];
            
            [shared setObject:array forKey:@"topShelfChannels"];
            [shared synchronize];
            _arrayTopShelfChannels = nil;
        }
        
    } else {
        
        NSDictionary *objectToAdd = @{@"channelName" : broadcastItem.channelName, @"channelId" : broadcastItem.channelID, @"channelURLImage" : [broadcastItem.channelLogoURL absoluteString], @"channelPlayURL" : playURL};
        NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:objectToAdd];
        
        [shared setObject:@[encodedObject] forKey:@"topShelfChannels"];
        [shared synchronize];
        _arrayTopShelfChannels = nil;
    }
}

@end
