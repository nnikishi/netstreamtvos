//
//  LBDBroadcast.h
//  NetstreamTVOS
//
//  Created by Dmitry on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LBDBroadcast : NSObject

@property (nonatomic, strong, nonnull, readonly) NSNumber *ID;
@property (nonatomic, strong, nonnull, readonly) NSNumber *startTime;
@property (nonatomic, strong, nonnull, readonly) NSNumber *endTime;
@property (nonatomic, copy, nonnull, readonly) NSString *channelID;
@property (nonatomic, copy, nonnull, readonly) NSString *channelName;

@property (nonatomic, copy, nonnull, readonly) NSString *title;

+ (nonnull instancetype)broadcastWithDictionary:(nonnull NSDictionary *)aDictionary;
+ (nonnull instancetype)broadcastWithDictionary:(nonnull NSDictionary *)aDictionary andChannelName:(nonnull NSString *)channelName;
- (nonnull NSString *)startTimeRepresentation;
- (nonnull NSString *)endTimeRepresentation;
- (nonnull NSURL *)broadcastThumbnailURL;
- (nonnull NSURL *)channelLogoURL;

@end


