//
//  LBDChannel.m
//  NetstreamTVOS
//
//  Created by Dmitry on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import "LBDChannel.h"

@interface LBDChannel()

@property (nonatomic, copy, readwrite, nonnull) NSString *ID;
@property (nonatomic, assign, readwrite) BOOL mobileAvailable;
@property (nonatomic, copy, readwrite, nonnull) NSString *channelName;

@end

@implementation LBDChannel

+ (nonnull instancetype)channelWithDictionary:(nonnull NSDictionary *)aDictionary {
	LBDChannel *channel     = [LBDChannel new];
	channel.ID              = aDictionary[@"channelId"];
	channel.mobileAvailable = [((NSNumber *)aDictionary[@"ottAvailable"]) boolValue];
    channel.channelName = aDictionary[@"name"];

	return channel;
}

@end
