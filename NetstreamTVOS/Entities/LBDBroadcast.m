//
//  LBDBroadcast.m
//  NetstreamTVOS
//
//  Created by Dmitry on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import "LBDBroadcast.h"
#import "NSDate+LBD.h"

static NSString *const kLBDBroadcastThumbnailURLFormat = @"http://u8182.vdc.netstream.com:8081/fb/epg/%@?tenantId=1";
static NSString *const kLBDBroadcastChannelLogoURLFormat = @"http://u8182.vdc.netstream.com:8081/fb/channel-logo/%@/scale/110/110?variant=dark";

@interface LBDBroadcast ()
@property (nonatomic, strong, nonnull, readwrite) NSNumber *ID;
@property (nonatomic, strong, nonnull, readwrite) NSNumber *startTime;
@property (nonatomic, strong, nonnull, readwrite) NSNumber *endTime;
@property (nonatomic, copy, nonnull, readwrite) NSString *channelID;
@property (nonatomic, copy, nonnull, readwrite) NSString *title;
@property (nonatomic, copy, nonnull, readwrite) NSString *channelName;
@end

@implementation LBDBroadcast

+ (nonnull instancetype)broadcastWithDictionary:(nonnull NSDictionary *)aDictionary {
	LBDBroadcast *broadcast = [LBDBroadcast new];
	broadcast.ID         = aDictionary[@"broadcastId"];
	broadcast.startTime  = aDictionary[@"startTime"];
	broadcast.endTime    = aDictionary[@"endTime"];
	broadcast.channelID  = aDictionary[@"channelId"];
	broadcast.title      = aDictionary[@"title"];
	return broadcast;
}

+ (nonnull instancetype)broadcastWithDictionary:(nonnull NSDictionary *)aDictionary andChannelName:(nonnull NSString *)channelName {
    LBDBroadcast *broadcast = [self broadcastWithDictionary:aDictionary];
    broadcast.channelName = channelName;
    return broadcast;
}

- (nonnull NSString *)startTimeRepresentation {
	NSTimeInterval startTimeInterval = [NSDate lbd_intervalFromMilliseconds:[_startTime doubleValue]];
	NSString *timeRepresentation = [[NSDate dateWithTimeIntervalSince1970:startTimeInterval] stringFromDateWithFormat:@"H:mm"];
	return timeRepresentation;
}

- (nonnull NSString *)endTimeRepresentation {
	NSTimeInterval endTimeInterval = [NSDate lbd_intervalFromMilliseconds:[_endTime doubleValue]];
	NSString *timeRepresentation = [[NSDate dateWithTimeIntervalSince1970:endTimeInterval] stringFromDateWithFormat:@"H:mm"];
	return timeRepresentation;
}

- (nonnull NSURL *)broadcastThumbnailURL {
	NSString *thumbnailURLString = [NSString stringWithFormat:kLBDBroadcastThumbnailURLFormat, [_ID stringValue]];
	return [NSURL URLWithString:thumbnailURLString];
}

- (nonnull NSURL *)channelLogoURL {
	NSString *logoURLString = [NSString stringWithFormat:kLBDBroadcastChannelLogoURLFormat, _channelID];
	return [NSURL URLWithString:logoURLString];
}

@end
