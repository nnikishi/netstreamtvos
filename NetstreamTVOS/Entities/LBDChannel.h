//
//  LBDChannel.h
//  NetstreamTVOS
//
//  Created by Dmitry on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LBDChannel : NSObject

@property (nonatomic, copy, readonly, nonnull) NSString *ID;
@property (nonatomic, assign, readonly) BOOL mobileAvailable;
@property (nonatomic, copy, readonly, nonnull) NSString *channelName;

+ (nonnull instancetype)channelWithDictionary:(nonnull NSDictionary *)aDictionary;

@end
