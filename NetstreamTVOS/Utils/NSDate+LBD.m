//
//  NSDate+LBD.m
//  NetstreamTVOS
//
//  Created by Dmitry on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import "NSDate+LBD.h"

@implementation NSDate (LBD)

#pragma mark -
#pragma mark Static

+ (double)lbd_millisecondsFromInterval:(NSTimeInterval)anInterval {
	return anInterval * 1000;
}

+ (NSTimeInterval)lbd_intervalFromMilliseconds:(double)aMilliseconds {
	return aMilliseconds / 1000;
}

#pragma mark -
#pragma mark Instance

- (NSDateFormatter *)dateFormatter {
	static NSDateFormatter *s_dateFormatter = nil;
	static dispatch_once_t oncePredicate;
	dispatch_once(&oncePredicate, ^{
		s_dateFormatter = [[NSDateFormatter alloc] init];
		[s_dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]]];
		[s_dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
	});
	return s_dateFormatter;
}

- (NSString *)stringFromDateWithFormat:(NSString *)format {
	NSDateFormatter *formatter = [self dateFormatter];
	[formatter setDateFormat:format];
	NSString *formattedDate = [formatter stringFromDate:self];
	return formattedDate;
}

@end
