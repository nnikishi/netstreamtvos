//
//  NSDate+LBD.h
//  NetstreamTVOS
//
//  Created by Dmitry on 12/3/15.
//  Copyright © 2015 Netstream AG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (LBD)

+ (double)lbd_millisecondsFromInterval:(NSTimeInterval)anInterval;
+ (NSTimeInterval)lbd_intervalFromMilliseconds:(double)aMilliseconds;
- (NSString *)stringFromDateWithFormat:(NSString *)format;

@end
